import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddBannerComponent } from './banner/add-banner/add-banner.component';
import { AddCampaignComponent } from './campaigns/add-campaign/add-campaign.component';
import { ListBannersComponent } from './banner/list-banners/list-banners.component';
import { ListCampaignsComponent } from './campaigns/list-campaigns/list-campaigns.component';
import { EditBannerComponent } from './banner/edit-banner/edit-banner.component';
import { EditCampaignComponent } from './campaigns/edit-campaign/edit-campaign.component';
import { HomepageComponent } from './homepage/homepage.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'banners', component: ListBannersComponent },
  { path: 'banners/add', component: AddBannerComponent },
  { path: 'banners/edit/:id', component: EditBannerComponent },
  { path: 'campaigns', component: ListCampaignsComponent },
  { path: 'campaigns/add', component: AddCampaignComponent },
  { path: 'campaigns/edit/:id', component: EditCampaignComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
