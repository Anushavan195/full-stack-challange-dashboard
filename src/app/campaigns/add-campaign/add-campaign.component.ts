import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { BannerDto } from 'src/app/banner/dto/banner.dto';
import { ActiveHoursDto } from '../dto/active-hours.dto';
import { ActiveHour } from '../models/active-hour.model';

@Component({
  selector: 'app-add-campaign',
  templateUrl: './add-campaign.component.html',
  styleUrls: ['./add-campaign.component.scss']
})
export class AddCampaignComponent implements OnInit {

  errorMessage = ''

  constructor(private http: HttpClient, private router: Router) { }

  async ngOnInit(): Promise<void> {
    this.banners = await this.http.get('banner').toPromise() as BannerDto[]
  }

  time = ''

  activeHours: ActiveHour[] = []

  campaignName = new FormControl('');

  banners: BannerDto[] = [];

  selectedBanners: BannerDto[] = [];

  async onSubmit(e: any) {
    e.preventDefault();

    try {
      await this.http.post('campaign', {
        name: this.campaignName.value,
        ranges: this.activeHours.map(ah => ({
          start: ActiveHour.formatTime(ah.start),
          end: ActiveHour.formatTime(ah.end)
        })),
        bannersIds: this.selectedBanners.map(b => b.id)
      }).toPromise()

      this.router.navigate(['/campaigns']);
    } catch (err: any) {
      this.errorMessage = typeof err.error.message == 'string' ? err.error.message : err.error.message || '';
    }
  }

  addActiveHoursRange() {
    this.activeHours.push(new ActiveHour())
  }

  removeActiveHour(activeHour: ActiveHour) {
    this.activeHours = this.activeHours.filter(ah => ah.id != activeHour.id)
  }
}
