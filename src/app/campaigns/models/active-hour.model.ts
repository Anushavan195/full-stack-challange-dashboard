import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { Guid } from 'guid-typescript';

const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;


export class ActiveHour {
    start: NgbTimeStruct | null = null

    end: NgbTimeStruct | null = null

    id: Guid;

    constructor() {
        this.id = Guid.create(); // ==> b77d409a-10cd-4a47-8e94-b0cd0ab50aa1
    }

    static formatTime(time: NgbTimeStruct | null) {
        return time != null ? `${pad(time.hour)}:${pad(time.minute)}` : null;
    }

    static fromModel(value: string | null): NgbTimeStruct | null {
        if (!value) {
            return null;
        }
        const split = value.split(':');
        return {
            hour: parseInt(split[0], 10),
            minute: parseInt(split[1], 10),
            second: parseInt(split[2], 10)
        };
    }
}
