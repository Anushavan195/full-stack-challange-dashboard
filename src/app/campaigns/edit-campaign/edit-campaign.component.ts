import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BannerDto } from 'src/app/banner/dto/banner.dto';
import { CampaignDto } from '../dto/campaign.dto';
import { ActiveHour } from '../models/active-hour.model';

@Component({
  selector: 'app-edit-campaign',
  templateUrl: './edit-campaign.component.html',
  styleUrls: ['./edit-campaign.component.scss']
})
export class EditCampaignComponent implements OnInit {

  errorMessage = ''

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit(): Promise<void> {
    this.route
      .paramMap.subscribe(async (paramsMap: any) => {
        const campaignId = paramsMap.params['id'];

        if (campaignId) {
          this.campaign = await this.http.get(`campaign/${campaignId}`).toPromise() as CampaignDto

          this.campaignName = new FormControl(this.campaign.name);

          this.selectedBanners = this.campaign.banners;

          if (this.campaign && this.campaign.activeTimeRanges)
            this.activeHours = this.campaign.activeTimeRanges.map(atr => {
              return {
                start: ActiveHour.fromModel(atr.start),
                end: ActiveHour.fromModel(atr.end),
              } as ActiveHour
            });
        }
      })

    this.banners = await this.http.get('banner').toPromise() as BannerDto[]
  }

  time = ''

  activeHours: ActiveHour[] = []

  campaign = new CampaignDto()

  campaignName = new FormControl('');

  banners: BannerDto[] = [];

  selectedBanners: BannerDto[] = [];

  async onSubmit(e: any) {
    e.preventDefault();

    try {
      await this.http.patch(`campaign/${this.campaign.id}`, {
        name: this.campaignName.value,
        ranges: this.activeHours.map(ah => ({
          start: ActiveHour.formatTime(ah.start),
          end: ActiveHour.formatTime(ah.end)
        })),
        bannersIds: this.selectedBanners.map(b => b.id)
      }).toPromise()

      this.router.navigate(['/campaigns']);
    } catch (err: any) {
      this.errorMessage = typeof err.error.message == 'string' ? err.error.message : err.error.message || '';
    }
  }

  addActiveHoursRange() {
    this.activeHours.push(new ActiveHour())
  }

  removeActiveHour(activeHour: ActiveHour) {
    this.activeHours = this.activeHours.filter(ah => ah.id != activeHour.id)
  }

}
