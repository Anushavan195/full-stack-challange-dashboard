import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CampaignDto } from '../dto/campaign.dto';

@Component({
  selector: 'list-campaigns',
  templateUrl: './list-campaigns.component.html',
  styleUrls: ['./list-campaigns.component.scss']
})
export class ListCampaignsComponent implements OnInit {

  campaigns: CampaignDto[] = []

  constructor(private http: HttpClient, private route: Router) { }

  async ngOnInit(): Promise<void> {
    this.campaigns = await this.http.get('campaign/list/all').toPromise() as CampaignDto[]
  }

  editCompaign(campaign: any) {
    this.route.navigate([`/campaigns/edit/${campaign.id}`])
  }

  async deleteCampaign(campaign: any) {
    await this.http.delete(`campaign/${campaign.id}`).toPromise()

    this.campaigns = await this.http.get('campaign/list/all').toPromise() as CampaignDto[]
  }
}
