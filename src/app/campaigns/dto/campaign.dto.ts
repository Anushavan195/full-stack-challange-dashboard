import { BannerDto } from "src/app/banner/dto/banner.dto"
import { ActiveHoursDto } from "./active-hours.dto"

export class CampaignDto {
    id = 0

    name = ''

    activeTimeRanges?: ActiveHoursDto[] = []

    banners: BannerDto[] = []
}