import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CampaignDto } from '../campaigns/dto/campaign.dto';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  constructor(private http: HttpClient) { }

  campaigns: CampaignDto[] = []

  async ngOnInit(): Promise<void> {
    this.campaigns = await this.http.get('campaign').toPromise() as CampaignDto[];
  }

}
