import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIInterceptor } from './http.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddBannerComponent } from './banner/add-banner/add-banner.component';
import { AddCampaignComponent } from './campaigns/add-campaign/add-campaign.component';
import { EditBannerComponent } from './banner/edit-banner/edit-banner.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ListCampaignsComponent } from './campaigns/list-campaigns/list-campaigns.component';
import { ListBannersComponent } from './banner/list-banners/list-banners.component';
import { EditCampaignComponent } from './campaigns/edit-campaign/edit-campaign.component';
import { HomepageComponent } from './homepage/homepage.component';

@NgModule({
  declarations: [
    AppComponent,
    ListBannersComponent,
    ListCampaignsComponent,
    AddBannerComponent,
    AddCampaignComponent,
    EditBannerComponent,
    EditCampaignComponent,
    HomepageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgSelectModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: APIInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
