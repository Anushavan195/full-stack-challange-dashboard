import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CampaignDto } from 'src/app/campaigns/dto/campaign.dto';
import { BannerDto } from '../dto/banner.dto';

@Component({
  selector: 'app-edit-banner',
  templateUrl: './edit-banner.component.html',
  styleUrls: ['./edit-banner.component.scss']
})
export class EditBannerComponent implements OnInit {

  banner = new BannerDto();

  errorMessage = ''

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute,) { }

  async ngOnInit(): Promise<void> {
    this.route
      .paramMap.subscribe(async (paramsMap: any) => {
        const bannerId = paramsMap.params['id'];

        if (bannerId) {
          this.banner = await this.http.get(`banner/${bannerId}`).toPromise() as BannerDto

          this.bannerName = new FormControl(this.banner.name);
          this.bannerText = new FormControl(this.banner.text);
        }
      })
  }

  bannerName = new FormControl('');
  bannerText = new FormControl('');

  async onSubmit(e: any) {
    e.preventDefault();

    try {
      await this.http.patch(`banner/${this.banner.id}`, {
        name: this.bannerName.value,
        text: this.bannerText.value
      }).toPromise()

      this.router.navigate(['/banners']);
    } catch (err: any) {
      this.errorMessage = typeof err.error.message == 'string' ? err.error.message : err.error.message || '';
    }
  }
}
