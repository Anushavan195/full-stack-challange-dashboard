import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-banner',
  templateUrl: './add-banner.component.html',
  styleUrls: ['./add-banner.component.scss']
})
export class AddBannerComponent implements OnInit {

  errorMessage = ''

  constructor(private http: HttpClient, private route: Router) { }

  ngOnInit(): void {
  }

  bannerName = new FormControl('');
  bannerText = new FormControl('');

  async onSubmit(e: any) {
    e.preventDefault();

    try {
      await this.http.post('banner', {
        name: this.bannerName.value,
        text: this.bannerText.value
      }).toPromise()

      this.route.navigate(['/banners']); 
    } catch (err: any) {
      this.errorMessage = typeof err.error.message == 'string' ? err.error.message : err.error.message || '';
    }
  }

}
