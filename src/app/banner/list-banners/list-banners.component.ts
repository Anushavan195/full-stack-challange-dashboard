import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'list-banners',
  templateUrl: './list-banners.component.html',
  styleUrls: ['./list-banners.component.scss']
})
export class ListBannersComponent implements OnInit {

  banners: any = []

  constructor(private http: HttpClient, private route: Router) { }

  async ngOnInit(): Promise<void> {
    this.banners = await this.http.get('banner').toPromise()
  }

  editBanner(banner: any) {
    this.route.navigate([`/banners/edit/${banner.id}`])
  }

  async deleteBanner(banner: any) {
    await this.http.delete(`banner/${banner.id}`).toPromise()

    this.banners = await this.http.get('banner').toPromise()
  }
}
