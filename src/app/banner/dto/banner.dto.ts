import { CampaignDto } from "src/app/campaigns/dto/campaign.dto"

export class BannerDto {
    id = 0

    name = ''
    
    text = ''

    campaigns: CampaignDto[] = []
}